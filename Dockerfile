FROM openjdk:8-jdk-alpine as builder

RUN apk add --update bash && rm -rf /var/cache/apk/*
# create common group for nerds
RUN addgroup -S -g 1000 nerds \
&& adduser -D -S -G nerds -u 1000 -s /bin/sh nerd \
&& mkdir /home/nerds \

USER nerd
WORKDIR /home/nerds

COPY ./ /home/nerds/image
RUN chgrp nerds /home/nerds \
&&  chmod g+rwx /home/nerds

RUN ls /home/nerds/image
RUN chmod +x /home/nerds/image/gradlew
RUN cd /home/nerds/image && ./gradlew bootRepackage


FROM openjdk:8-jdk-alpine as app

RUN mkdir -p /usr/src/image
COPY --from=builder /home/nerds/image/build/libs/app.jar /usr/src/image
WORKDIR /usr/src/image
RUN ls -la
CMD ["java", "-jar", "app.jar"]
