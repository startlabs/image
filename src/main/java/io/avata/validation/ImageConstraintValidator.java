package io.avata.validation;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Paul Daniel Iway
 */
public class ImageConstraintValidator implements ConstraintValidator<Image, MultipartFile> {
  Image image;
  @Override
  public void initialize(Image image) {
    this.image = image;
  }

  @Override
  public boolean isValid(MultipartFile file, ConstraintValidatorContext constraintValidatorContext) {
    boolean valid;
    String mimeType = file.getContentType();
    switch (mimeType) {
      case "image/png":
      case "image/gif":
      case "image/jpg":
      case "image/jpeg":
        valid = true;
        break;
      default:
        valid = false;
    }
    return valid;
  }
}
