package io.avata.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Paul Daniel Iway
 */

@Target( { METHOD, FIELD, ANNOTATION_TYPE, PARAMETER })
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = ImageConstraintValidator.class)
public @interface Image {
  String message() default "Invalid image";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
