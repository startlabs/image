package io.avata.service;

import io.avata.model.Image;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * Created by paul on 8/5/16.
 */
@Component("keyGen")
public class ImageKeyGenerator implements KeyGenerator {
  @Override
  public Object generate(Object o, Method method, Object... objects) {
    StringBuilder key = new StringBuilder();
    for (Object param: objects) {
      if (param instanceof Map) {
        key.append(getMapKey((Map<String, String>) param));
      }
      if (param instanceof Image) {
        key.append(String.valueOf((Image)param).hashCode());
      }

    }
    return key.toString();
  }

  private String getMapKey(Map<String, String> map) {
    StringBuilder key = new StringBuilder();
    for (Map.Entry<String, String> entry : map.entrySet()) {
      key.append(entry.getKey());
      key.append(":");
      key.append(entry.getValue());
    }
    return key.toString();
  }
}
