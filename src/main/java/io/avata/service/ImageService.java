package io.avata.service;

import io.avata.model.Image;
import io.avata.model.ImageBuilder;
import io.avata.model.ImageBuilderFactory;
import io.avata.processor.ImageProcessor;
import io.avata.utils.ImageUtil;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.imaging.ImageReadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author Paul Daniel Iway
 */
@Component("imageService")
@Validated
public class ImageService {

  private final Logger logger =
      (Logger) LoggerFactory.getLogger(ImageService.class);

  @Autowired
  private ImageManipulationService manipulator;


  @Autowired
  private ImageUtil imageUtil;

  @Autowired
  private ProviderService providerService;

  public Image putImage(@io.avata.validation.Image MultipartFile upload) throws IOException, ImageReadException {

    File uploadedFile = ((DiskFileItem) ((CommonsMultipartFile) upload).getFileItem()).getStoreLocation();

    ImageBuilder builder = ImageBuilderFactory.from(upload.getInputStream(), upload.getOriginalFilename());

    return this.providerService.createImage(builder.build());
  }

  @Cacheable("raw-images")
  public Image getImage(String filename) {
    return this.providerService.getImage(filename, "");
  }

  public byte[] fromMap(Map<String, String[]> parameterMap, Image image) throws IOException {
    return this.imageUtil.toByteArray(this.bufferedImageFromMap(parameterMap,
                                                                image), image.getFormat());
  }

  public BufferedImage bufferedImageFromMap(Map<String, String[]> parameterMap, Image image) {
    if (parameterMap.isEmpty()) {
      return image.getBufferedImage();
    }
    Map<String, String> flatMap = flattenMap(parameterMap);
    return fromFlatMap(flatMap, image);
  }

  @Cacheable(cacheNames = "images", keyGenerator = "keyGen")
  public BufferedImage fromFlatMap(Map<String, String> map, Image image) {
    ImageProcessor processor = this.manipulator.fromMap(map);
    BufferedImage processedImage =  processor.process(image.getBufferedImage());
    return processedImage;
  }

  /**
   *
   * @param parameterMap
   * @return the flattened hash map of a query string
   */
  public LinkedHashMap<String, String> flattenMap(Map<String, String[]> parameterMap) {
    LinkedHashMap<String, String> flatMap = new LinkedHashMap<>();
    parameterMap.forEach((name, value) -> flatMap.put(name, value[0]));
    return flatMap;
  }

  public void setManipulator(ImageManipulationService manipulator) {
    this.manipulator = manipulator;
  }

  public void setImageUtil(ImageUtil imageUtil) {
    this.imageUtil = imageUtil;
  }
}
