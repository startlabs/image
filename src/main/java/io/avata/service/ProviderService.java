package io.avata.service;

import io.avata.controller.response.InternalErrorException;
import io.avata.controller.response.NotFoundException;
import io.avata.imageStore.ImageNotFoundException;
import io.avata.imageStore.ImageStore;
import io.avata.imageStore.ImageStoreException;
import io.avata.model.Image;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Supplies the image files to the ImageService
 *
 * @author Paul Daniel Iway
 */
@Service
public class ProviderService {

  private final ImageStore store;

  @Autowired
  public ProviderService(@Qualifier("localStore") ImageStore store) {
    this.store = store;
  }

  public Image getImage(final String name, final String prefix) {
    try {
      return this.store.read(name, prefix);
    } catch (ImageNotFoundException e) {
      throw new NotFoundException();
    } catch (ImageStoreException e) {
      throw new InternalErrorException(e);
    }
  }

  public Image createImage(Image image) {
    return this.store.create(image);
  }
}
