package io.avata.controller;


import io.avata.controller.response.InternalErrorException;
import io.avata.imageStore.ImageStoreException;
import io.avata.model.Image;
import io.avata.service.ImageService;
import org.apache.commons.imaging.ImageReadException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

/**
 * @author Paul Daniel Iway
 */
@RestController
public class ImageController {

  private static final Logger logger =
      (Logger) LoggerFactory.getLogger(ImageController.class);

  @Autowired
  private ImageService imageService;

  @RequestMapping(value = "/{filename:.+}.json", produces = "application/json; charset=utf-8")
  @ResponseBody
  public Image meta(@PathVariable String filename) {
    return this.imageService.getImage(filename);
  }

  @RequestMapping(value = "/{filename:.+}")
  @ResponseBody
  public BufferedImage image(@PathVariable String filename, HttpServletRequest request) {
    try {
      Image image = this.imageService.getImage(filename);
      Map<String, String> flatParam = this.imageService.flattenMap(request.getParameterMap());
      return this.imageService.fromFlatMap(flatParam, image);
    } catch (ImageStoreException e) {
      throw new InternalErrorException(e);
    }
  }

  @RequestMapping(value="/", method= RequestMethod.POST, produces = "application/json; charset=utf-8")
  public @ResponseBody Image handleFileUpload(@RequestPart("file") MultipartFile file){
    try {
      return this.imageService.putImage(file);
    } catch (IOException | ImageReadException e) {
      throw new InternalErrorException(e);
    }
  }
}
