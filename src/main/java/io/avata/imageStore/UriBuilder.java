package io.avata.imageStore;

import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by Paul Daniel Iway on 4/26/16.
 */
public class UriBuilder {

  private String scheme;
  private String host;


  private ArrayList<String> paths;

  private UriBuilder(String uri) {
    this.paths = new ArrayList<>();
    this.parse(uri);
  }

  private void parse(@NotNull String str) {
    this.parse(URI.create(str));
  }

  private void parse(URI uri) {
    this.scheme = uri.getScheme();
    this.host = uri.getHost();

    String pathString = uri.getPath();

    for (String path : pathString.split("/")) {
      this.paths.add(path);
    }
  }

  public UriBuilder addPath(String path) {
    if (path.contains("/")) {
      for (String p : path.split("/")) {
        if (!p.isEmpty()) {
          this.paths.add(p);
        }
      }
    } else {
      this.paths.add(path);
    }
    return this;
  }

  public URI build() throws URISyntaxException {
    return new URI(this.scheme, this.host, this.getPathString(), null);
  }

  private String getPathString() {
    StringBuilder sb = new StringBuilder();
    for (String path : this.paths) {
      sb.append(path);
      sb.append("/");
    }

    // removes trailing slash
    sb.deleteCharAt(sb.length() - 1);
    return sb.toString();
  }


  public static UriBuilder getBuilder(String uri) {
    return new UriBuilder(uri);
  }

}
