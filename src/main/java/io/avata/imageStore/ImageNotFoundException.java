package io.avata.imageStore;

/**
 * @author Paul Daniel Iway
 */
public class ImageNotFoundException extends InvalidImagePathException {
  public ImageNotFoundException(String path) {
    super(path);
  }

  public ImageNotFoundException(String path, String message) {
    super(path, "Image not found at: " . concat(path));
  }
}
