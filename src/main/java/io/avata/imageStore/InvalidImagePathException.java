package io.avata.imageStore;

/**
 * @author Paul Daniel Iway
 */
public class InvalidImagePathException extends ImageStoreException {

  private final String path;

  public InvalidImagePathException(String path) {
    this("Invalid path: ", path);
  }

  public InvalidImagePathException(String path, String message) {
    super(message);
    this.path = path;
  }

  public String getPath() {
    return path;
  }
}
