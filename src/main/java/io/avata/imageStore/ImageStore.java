package io.avata.imageStore;

import io.avata.model.Image;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author Paul Daniel Iway
 */
public interface ImageStore {

  Image create(Image image);

  Image read(String name, String prefix);

  Image read(String name);

  Image update(Image image);

  Image delete(Image image);

  URI getRoot();

  String getScheme();

  default URI getImageUri(final String name) throws InvalidImagePathException {
    return this.getImageUri(name, null);
  }

  default URI getImageUri(final String name, final String prefix) throws InvalidImagePathException {

    UriBuilder builder = UriBuilder.getBuilder(this.getRoot().toString());

    if (prefix != null) {
      builder.addPath(prefix);
    }

    builder.addPath(name);

    try {
      return builder.build();
    } catch (URISyntaxException e) {
      throw new InvalidImagePathException(builder.toString(), e.getMessage());
    }
  }
}
