package io.avata.imageStore;

/**
 * @author Paul Daniel Iway
 */
public class InvalidStoreRootException extends ImageStoreException {
  public InvalidStoreRootException() {
  }

  public InvalidStoreRootException(String message) {
    super(message);
  }

  public InvalidStoreRootException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidStoreRootException(Throwable cause) {
    super(cause);
  }

  protected InvalidStoreRootException(String message, Throwable cause,
                                      boolean enableSuppression, boolean
                                          writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
