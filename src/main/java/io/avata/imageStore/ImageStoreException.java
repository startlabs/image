package io.avata.imageStore;

/**
 * @author Paul Daniel Iway
 */
public class ImageStoreException extends RuntimeException {
  public ImageStoreException() {
    super();
  }

  public ImageStoreException(String message) {
    super(message);
  }

  public ImageStoreException(String message, Throwable cause) {
    super(message, cause);
  }

  public ImageStoreException(Throwable cause) {
    super(cause);
  }

  protected ImageStoreException(String message, Throwable cause, boolean
      enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
