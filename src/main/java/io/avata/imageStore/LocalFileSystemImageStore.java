package io.avata.imageStore;

import io.avata.model.Image;
import io.avata.model.ImageBuilder;
import io.avata.model.ImageBuilderFactory;
import org.apache.commons.imaging.ImageReadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @author Paul Daniel Iway
 */
@Component("localStore")
public class LocalFileSystemImageStore implements ImageStore, ImageBuilderFactory {

  private final URI root;

  @Autowired
  public LocalFileSystemImageStore(@Value("${image.path}") String root) throws URISyntaxException {
    this(new URI(root));
  }

  public LocalFileSystemImageStore(URI uri) {
    if (uri.getScheme() == null) {
      throw new InvalidStoreRootException("Scheme cannot be null");
    }
    if (!uri.getScheme().equals(this.getScheme())) {
      throw new InvalidStoreRootException("Unknown scheme");
    }
    this.root = uri;
  }

  @Override
  public Image create(Image image) {
    File f = new File(this.getImageUri(image.getName()));
    try {
      ImageIO.write(image.getBufferedImage(), image.getFormat(), f);
    } catch (IOException e) {
      throw new ImageStoreException(e);
    }
    return image;
  }

  @Override
  public Image read(String name) {
    return read(name, null);
  }

  @Override
  public Image read(String name, String prefix) {

    File imageFile = new File(this.getImageUri(name, prefix));

    try {

      ImageBuilder builder = ImageBuilderFactory.from(imageFile);
      return builder.build();
    } catch (FileNotFoundException e) {
      throw new ImageNotFoundException(imageFile.getAbsolutePath());
    } catch (IOException | ImageReadException e) {
      throw new ImageStoreException(e);
    }
  }

  @Override
  public Image update(Image image) {
    throw new UnsupportedOperationException();
  }

  @Override
  public Image delete(Image image) {
    throw new UnsupportedOperationException();
  }

  @Override
  public URI getRoot() {
    return this.root;
  }

  @Override
  public String getScheme() {
    return "file";
  }
}
