/**
 * Classes that allow manipulation of image files.
 * @author Paul Daniel Iway
 */
package io.avata.processor;