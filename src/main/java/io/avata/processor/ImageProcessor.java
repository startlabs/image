package io.avata.processor;

import io.avata.processor.commands.Crop;
import io.avata.processor.commands.ImageProcessorCommand;
import io.avata.processor.commands.ProcessorEngineNotSetException;
import io.avata.processor.commands.Resize;

import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Holds and executes commands/steps for manipulating an image. Commands are
 * executed in the order that they were added.
 *
 * @author Paul Daniel Iway
 */
public class ImageProcessor {

  /**
   * Holds the commands which will be executed.
   */
  private PriorityQueue<ImageProcessorCommand> commands;

  /**
   * The image manipulation engine for the class to use.
   */
  private ProcessorEngine engine;

  public ImageProcessor(ProcessorEngine engine) {
    this.engine = engine;
    // we want to use reverse(desc) of natural sorting (asc)
    // because we want to execute commands with higher priority first
    this.commands = new PriorityQueue<>(Collections.reverseOrder());
  }

  /**
   * Creates an instance of a resize command.
   * @param width the new width to set the image to.
   * @param height the new height to set the image to.
   * @return An instance of the Resize command.
   */
  public static Resize resize(int width, int height) {
    return new Resize(width, height);
  }

  /**
   * Creates an instance of a resize command.
   * @param scale the percentage of the image size
   * @return An instance of the Resize command.
   */
  public static Resize resize(double scale) {
    return new Resize(scale);
  }

  public static Crop crop(int width, int height, int x, int y) {
    return new Crop(width, height, x, y);
  }
  /**
   * Adds a command to be executed by the ImageProcessor instance. The commands
   * are stored in a list so that commands are executed in the order that they
   * were added.
   * @param command The command to add
   */
  public void addCommand(ImageProcessorCommand command) {
    command.setEngine(this.engine);
    this.commands.offer(command);
  }

  /**
   * Determines whether a command instance has been added.
   * @param command The command to determine if it was added.
   * @return true if command has been added; false if not.
   */
  public boolean commandExists(ImageProcessorCommand command) {
    return this.commands.contains(command);
  }

  public Queue<ImageProcessorCommand> getCommands() {
    return commands;
  }

  public BufferedImage process(BufferedImage image) {
    for (ImageProcessorCommand c : this.commands) {
      c.setEngine(this.engine);
      image = c.execute(image);
    }

    return image;
  }
}
