package io.avata.processor.commands;

import io.avata.processor.ProcessorEngine;

import java.awt.image.BufferedImage;

/**
 * A single command to manipulate an image.
 * @author Paul Daniel Iway
 */
public interface ImageProcessorCommand extends Comparable<ImageProcessorCommand>{

  /**
   * Executes the command instructions.
   * @param image The image to be manipulated.
   * @return The manipulated image
   * @throws ProcessorEngineNotSetException Unchecked exception when no {@link ProcessorEngine}
   * has been set.
   */
  BufferedImage execute(BufferedImage image);

  /**
   * Sets the engine that alters the image.
   * @param engine
   */
  void setEngine(ProcessorEngine engine);

  /**
   * Gets the engine instance for this command.
   * @return
   */
  ProcessorEngine getEngine();

  /**
   * Does nothing but throw exception when the engine is not set.
   * @throws ProcessorEngineNotSetException
   */
  default void checkEngine() throws ProcessorEngineNotSetException {
    if(this.getEngine() == null) {
      throw new ProcessorEngineNotSetException();
    }
  }

  @Override
  boolean equals(Object o);
  static final int PRIORITY_ALWAYS_FIRST = 1;
  static final int PRIORITY_NONE = 0;

  int getPriority();

  @Override
  default int compareTo(ImageProcessorCommand o) {
    // if negative (this is less than o)
    // if zero, this is equal to o
    // if positive (this has more priority over o
    return this.getPriority() - o.getPriority();
  }
}
