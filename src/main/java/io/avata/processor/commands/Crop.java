package io.avata.processor.commands;

import io.avata.processor.ProcessorEngine;

import java.awt.image.BufferedImage;

/**
 * @author Paul Daniel Iway
 */
public class Crop implements ImageProcessorCommand{

  private ProcessorEngine engine;

  private int width;
  private int height;
  private int x;
  private int y;

  public Crop(int width, int height, int x, int y) {

    this.width = width;
    this.height = height;
    this.x = x;
    this.y = y;
  }

  @Override
  public BufferedImage execute(BufferedImage image) {
    return this.engine.crop(image, this.width, this.height, this.x, this.y);
  }

  @Override
  public void setEngine(ProcessorEngine engine) {
    this.engine = engine;
  }

  @Override
  public ProcessorEngine getEngine() {
    return this.engine;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Crop crop = (Crop) o;

    if (width != crop.width) return false;
    if (height != crop.height) return false;
    if (x != crop.x) return false;
    if (y != crop.y) return false;
    return true;
  }

  @Override
  public int getPriority() {
    return ImageProcessorCommand.PRIORITY_NONE;
  }

}
