package io.avata;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json
    .MappingJackson2HttpMessageConverter;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.config.annotation
    .ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation
    .WebMvcConfigurerAdapter;

import java.util.List;

/**
 * @author Paul Daniel Iway
 */

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

  @Override
  public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    //configurer.favorPathExtension(false);
  }

  @Override
  public void configureMessageConverters(List<HttpMessageConverter<?>>
                                               converters) {
    super.configureMessageConverters(converters);
    converters.add(new BufferedImageHttpMessageConverter());
    converters.add(new ByteArrayHttpMessageConverter());
    converters.add(new MappingJackson2HttpMessageConverter());
  }

  @Bean
  public MultipartResolver multipartResolver() {
    org.springframework.web.multipart.commons.CommonsMultipartResolver multipartResolver = new org.springframework.web.multipart.commons.CommonsMultipartResolver();
    return multipartResolver;
  }

  /*############################
   * Beans used in validation
   #############################*/
  @Bean
  public javax.validation.Validator localValidatorFactoryBean() {
    return new LocalValidatorFactoryBean();
  }

  @Bean
  public org.springframework.validation.beanvalidation.MethodValidationPostProcessor methodValidationPostProcessor() {
    return new org.springframework.validation.beanvalidation.MethodValidationPostProcessor();
  }
  /*
   * END Valdation beans
   */
}
