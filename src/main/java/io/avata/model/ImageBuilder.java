package io.avata.model;

import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.imaging.ImageReadException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * @author Paul Daniel Iway
 */
public class ImageBuilder implements ImageBuilderFactory{

  private ImageInfo imageInfo;

  private String name;
  private BufferedImage imageData;
  private long length;
  private long lastModified;

  protected ImageBuilder(File file, ImageInfo imageInfo) throws IOException, ImageReadException {
    if (file.isDirectory()) {
      throw new FileNotFoundException("Expected file but got directory:" + file.getPath());
    }
    this.imageInfo = imageInfo;
    length = file.length();
    imageData = ImageIO.read(file);
    this.lastModified = file.lastModified();
  }

  protected ImageBuilder(InputStream is,ImageInfo imageInfo, String fileName) throws IOException, ImageReadException {
    is.reset();
    length = is.available();
    this.imageInfo = imageInfo;
    BufferedImage bi = ImageIO.read(is);
    this.imageData = bi;
    this.name = fileName;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Image build() throws IOException, ImageReadException {

    return new Image(
        this.imageData,
        imageInfo.getWidth(),
        imageInfo.getHeight(),
        imageInfo.getFormat().getExtension(),
        imageInfo.getMimeType(),
        imageInfo.isTransparent(),
        imageInfo.isProgressive(),
        this.length,
        Optional.ofNullable(this.name).orElse(""),
        this.lastModified
    );
  }


  public String getFormatFromName(String name) {
    return name.substring(name.lastIndexOf('.') + 1, name.length());
  }
}
