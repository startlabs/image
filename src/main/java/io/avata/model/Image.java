package io.avata.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @author Paul Daniel Iway
 */
@JsonAutoDetect(getterVisibility = Visibility.NONE)
public class Image {

  private final BufferedImage image;

  private final int width;
  private final int height;
  private final String format;
  private final String mimeType;
  private final boolean isTransparent;
  private final boolean isProgressive;
  private final long length;
  private final String name;
  private final long lastModified;


  protected Image(final BufferedImage image,
                  final int width,
                  final int height,
                  final String format,
                  final String mimeType,
                  final boolean isTransparent,
                  final boolean isProgressive,
                  final long length,
                  final String name,
                  final long lastModified) {
    this.image = image;
    this.width = width;
    this.height = height;
    this.format = format;
    this.mimeType = mimeType;
    this.isTransparent = isTransparent;
    this.isProgressive = isProgressive;
    this.length = length;
    this.name = name;
    this.lastModified = lastModified;
  }

  @JsonSerialize
  @JsonProperty("length")
  public long length() {return this.length;}


  @JsonSerialize
  @JsonProperty("name")
  public String getName() {return this.name;}

  @JsonSerialize
  @JsonProperty("lastModified")
  public long lastModified() {return this.lastModified;}

  @JsonSerialize
  @JsonProperty("width")
  public int getWidth() {
    return this.width;
  }

  @JsonSerialize
  @JsonProperty("height")
  public int getHeight() {
    return this.height;
  }

  @JsonSerialize
  @JsonProperty("format")
  public String getFormat() {
    return format;
  }

  @JsonSerialize
  @JsonProperty("mimeType")
  public String getMimeType() {
    return mimeType;
  }

  @JsonSerialize
  @JsonProperty("transparent")
  public boolean isTransparent() {
    return isTransparent;
  }

  @JsonSerialize
  @JsonProperty("progressive")
  public boolean isProgressive() {
    return isProgressive;
  }

  public BufferedImage getBufferedImage() {
    return this.image;
  }

  @Override
  public int hashCode() {
    return (int)(Long.valueOf(this.name.hashCode()) + this.length);
  }
}