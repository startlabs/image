package io.avata.model;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

/**
 * @author Paul Daniel Iway
 */
public interface ImageBuilderFactory {

  static ImageBuilder from(File file) throws IOException, ImageReadException {
    return new ImageBuilder(file, Imaging.getImageInfo(file));
  }

  static ImageBuilder from(URI uri) throws IOException, ImageReadException {
    return from(new File(uri));
  }

  static ImageBuilder from(InputStream is, String fileName) throws IOException, ImageReadException {
    BufferedInputStream bis = new BufferedInputStream(is);
    bis.mark(is.available());
    return new ImageBuilder(bis, Imaging.getImageInfo(bis, fileName), fileName);
  }
}
