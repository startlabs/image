package io.avata;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.awt.image.BufferedImage;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * @author Paul Daniel Iway
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(SampleSimpleApplication.class)
@WebIntegrationTest
public class ScaleTest extends IntegrationTest {

  @Value("${local.server.port}")
  int port;

  @Before
  public void setUp() {
    RestAssured.port = this.port;
    RestAssured.baseURI = "http://localhost";
  }

  @Test
  public void fetch_image() {
    Response response = RestAssured.get("/scuba.jpg");
    assertEquals(200, response.getStatusCode());
    assertTrue(response.getContentType().contains("image/jpeg"));
  }

  @Test
  public void imageHeight_scale() {
    Response response = RestAssured.get("/scuba.jpg?h=200&scale=custom");
    BufferedImage image = this.toBufferedImage(response.getBody().asInputStream());
    assertEquals(200, image.getHeight());

    // although only the height is defined
    // the system should implicitly set the width to
    // match the image's aspect ratio
    assertEquals(267, image.getWidth());
  }

  @Test
  public void imageWidth_scale() {
    Response response = RestAssured.get("/scuba.jpg?w=350&scale=custom");
    BufferedImage image = this.toBufferedImage(response.getBody().asInputStream());
    assertEquals(350, image.getWidth());

    // height was is not set in the query string
    // width should be implicitly set to match aspect ratio of image
    assertEquals(263, image.getHeight());
  }

  @Test
  public void imageScale_width_height() {
    Response response = RestAssured.get("/scuba.jpg?scale&w=69&h=55");
    BufferedImage image = this.toBufferedImage(response.getBody().asInputStream());

    // image must match requested size
    assertEquals(69, image.getWidth());
    assertEquals(55, image.getHeight());
  }

  @Test
  public void imageScale_percentage() {
    // scuba.jpg is 960x720
    // test scaling image by half
    Response response = RestAssured.get("/scuba.jpg?scale=0.5");
    BufferedImage image = this.toBufferedImage(response.getBody().asInputStream());
    assertEquals(480, image.getWidth());
    assertEquals(360, image.getHeight());
  }

}
