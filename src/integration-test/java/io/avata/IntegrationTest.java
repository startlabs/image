package io.avata;

import org.junit.Ignore;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.fail;

/**
 * @author Paul Daniel Iway
 */
@Ignore
abstract class IntegrationTest {

  BufferedImage toBufferedImage(InputStream is) {
    BufferedImage image = null;
    try {
      image = ImageIO.read(is);
    } catch (IOException e) {
      fail(e.getMessage());
    }
    return image;
  }
}
