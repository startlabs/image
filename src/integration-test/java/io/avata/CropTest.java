package io.avata;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.awt.image.BufferedImage;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paul Daniel Iway on 4/29/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(SampleSimpleApplication.class)
@WebIntegrationTest
public class CropTest extends IntegrationTest {

  @Value("${local.server.port}")
  int port;

  @Before
  public void setUp() {
    RestAssured.port = this.port;
    RestAssured.baseURI = "http://localhost";
  }

  @Test
  public void crop_unscaled() {
    Response response = RestAssured.get("/scuba.jpg?crop&x=700&y=300&w=200&h=400");
    BufferedImage image = this.toBufferedImage(response.getBody().asInputStream());
    assertEquals(400, image.getHeight());
    assertEquals(200, image.getWidth());
  }

  @Test
  public void crop_scaled() {
    Response response = RestAssured.get("/scuba.jpg?crop&x=10&y=50&w=150&h=250&scale=0.5");
    BufferedImage image = this.toBufferedImage(response.getBody().asInputStream());
    assertEquals(250, image.getHeight());
    assertEquals(150, image.getWidth());
  }
}
