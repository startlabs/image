package io.avata;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.awt.image.BufferedImage;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import static com.jayway.restassured.RestAssured.given;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Created by Paul Daniel Iway on 4/29/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(SampleSimpleApplication.class)
@WebIntegrationTest
public class UploadTest extends IntegrationTest {

  @Value("${local.server.port}")
  int port;

  @Value("${image.path}")
  String uploadPath;

  @Before
  public void setUp() {
    RestAssured.port = this.port;
    RestAssured.baseURI = "http://localhost";
  }

  @Test
  public void upload() throws URISyntaxException {
    File f = new File(UploadTest.class.getResource("yoda.jpeg").toURI());
    Response response =
        given()
        .multiPart("file", f, "image/jpeg")
        .post("/");

    assertEquals(200, response.getStatusCode());

    URI uri = URI.create(uploadPath + "/yoda.jpeg");
    File upload = new File(uri);

    assertTrue(upload.exists());
    upload.deleteOnExit();
  }
}
