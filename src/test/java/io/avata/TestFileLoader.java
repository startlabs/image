package io.avata;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @author Paul Daniel Iway
 */
public interface TestFileLoader {
  default File load(String filename) throws URISyntaxException {
    URL url = TestFileLoader.class.getResource(filename);
    return new File(url.toURI());
  }
}
