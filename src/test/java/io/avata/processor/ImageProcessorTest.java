package io.avata.processor;

import io.avata.TestFileLoader;
import io.avata.processor.commands.Crop;
import io.avata.processor.commands.ImageProcessorCommand;
import io.avata.processor.commands.ProcessorEngineNotSetException;
import io.avata.processor.commands.Resize;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Paul Daniel Iway
 */
public class ImageProcessorTest implements TestFileLoader {

  private ImageProcessor processor;


  @Before
  public void setUp (){
    ProcessorEngine engine = new ImgScalrEngine();
    this.processor = new ImageProcessor(engine);
  }

  @Test
  public void testProcessorResize() {
    assertTrue(ImageProcessor.resize(10, 10) instanceof Resize);
  }

  @Test
  public void testProcessorAddCommand() {
    ImageProcessorCommand resize = ImageProcessor.resize(10, 10);
    this.processor.addCommand(resize);
    assertTrue(this.processor.commandExists(resize));
  }

  @Test
  public void commandNotExistsEmptyList() {
    ImageProcessorCommand resize = ImageProcessor.resize(10, 10);
    assertFalse(this.processor.commandExists(resize));
  }

  @Test
  public void commandNotExistsNonEmpty() {
    ImageProcessorCommand resize = ImageProcessor.resize(10, 10);
    this.processor.addCommand(resize);

    ImageProcessorCommand resize2 = ImageProcessor.resize(20, 20);
    assertFalse(this.processor.commandExists(resize2));
  }

  @Test
  public void testProcess() throws ProcessorEngineNotSetException {
    BufferedImage image = mock(BufferedImage.class);
    Resize resize = mock(Resize.class);
    this.processor.addCommand(resize);
    this.processor.process(image);
    verify(resize).execute(image);
  }

  @Test
  public void testProcessCommandEngineNotSet() {
    BufferedImage image = mock(BufferedImage.class);
    Resize resize = new Resize(10, 10);
    this.processor.addCommand(resize);
    resize.setEngine(null);
    this.processor.process(image);
    // assert that the process method recovers from
    // ProcessorEngineNotSetException by adding the engine when it
    // it has not been added yet
    assertNotNull(resize.getEngine());
  }

  @Test
  public void testCommandExists() {
    Resize resize = new Resize(10, 10);
    Resize resize2 = new Resize(10, 10);
    this.processor.addCommand(resize);
    assertTrue("similar commands exist", this.processor.commandExists(resize2));
  }

  @Test
  /**
   * Test that the commands are executed in a queueing order
   */
  public void testCommandQueue() throws ProcessorEngineNotSetException, URISyntaxException, IOException {

    BufferedImage image = ImageIO.read(load("chewie.jpg"));
    ImageProcessorCommand resize = spy(ImageProcessor.resize(0.5));
    ImageProcessorCommand crop = spy(ImageProcessor.crop(50, 50, 10, 10));

    this.processor.addCommand(crop);
    this.processor.addCommand(resize);
    InOrder inOrder = inOrder(resize, crop);

    this.processor.process(image);
    inOrder.verify(resize).execute(any(BufferedImage.class));
    inOrder.verify(crop).execute(any(BufferedImage.class));
  }
}