package io.avata.processor.commands;

import io.avata.processor.ProcessorEngine;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.awt.image.BufferedImage;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Paul Daniel Iway
 */

public class ResizeTest {
  @Rule
  public ExpectedException thrown = ExpectedException.none();
  Resize resize;
  BufferedImage image;
  ProcessorEngine mockEngine;

  @Before
  public void setUp() {
    this.mockEngine = mock(ProcessorEngine.class);

    this.image = mock(BufferedImage.class);
    when(this.image.getHeight()).thenReturn(100);
    when(this.image.getWidth()).thenReturn(100);

    this.resize = new Resize(10, 10);
    this.resize.setEngine(mockEngine);
  }

  @Test(expected = ProcessorEngineNotSetException.class)
  public void testExecuteNoEngine() throws ProcessorEngineNotSetException {
    this.resize.setEngine(null);
    this.resize.execute(null);
  }

  @Test
  public void testExecute() throws ProcessorEngineNotSetException {
    this.resize.execute(this.image);
    verify(this.mockEngine).resize(this.image, 10, 10,
                                   ProcessorEngine.Mode.EXPLICIT);
  }

  @Test
  public void testZeroHeight() throws ProcessorEngineNotSetException {
    this.resize = new Resize(10, 0);
    this.resize.setEngine(this.mockEngine);

    this.resize.execute(this.image);
    // Since one of the dimensions is zero, execute execute in implicit mode
    verify(this.mockEngine).resize(this.image, 10, 0, ProcessorEngine.Mode.IMPLICIT);
  }

  @Test
  public void testZeroWidth() throws ProcessorEngineNotSetException {
    this.resize = new Resize(0, 10);
    this.resize.setEngine(this.mockEngine);

    this.resize.execute(this.image);
    // Since one of the dimensions is zero, execute execute in implicit mode
    verify(this.mockEngine).resize(this.image, 0, 10, ProcessorEngine.Mode.IMPLICIT);

  }

  @Test
  public void TestResizePercentage() throws ProcessorEngineNotSetException {
    this.resize = new Resize(0.5);
    this.resize.setEngine(this.mockEngine);

    this.resize.execute(this.image);
    verify(this.mockEngine).resize(this.image, 50, 50,
                                   ProcessorEngine.Mode.IMPLICIT);
  }
}