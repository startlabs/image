package io.avata.imageStore;

import io.avata.model.Image;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

/**
 * @author Paul Daniel Iway
 */
public class ImageStoreTest {

  private ImageStore store;

  static final String FILE_STORE_ROOT = "file:///home/users/paul/Pictures";
  static final String HTTP_STORE_ROOT = "http://image.avata.io";

  @Before
  public void setup() throws URISyntaxException {
    this.store = this.getHttpStore();
  }

  public ImageStore getFileStore() throws URISyntaxException {
    return this.getStore(ImageStoreTest.FILE_STORE_ROOT);
  }

  private ImageStore getStore(String rootString) throws URISyntaxException {
    URI root = new URI(rootString);
    return new ImageStore() {
      @Override
      public Image create(Image image) {
        return null;
      }

      @Override
      public Image read(String name, String prefix) {
        return null;
      }

      @Override
      public Image read(String name) {
        return null;
      }

      @Override
      public Image update(Image image) {
        return null;
      }

      @Override
      public Image delete(Image image) {
        return null;
      }

      @Override
      public URI getRoot() {
        return root;
      }

      @Override
      public String getScheme() {
        return root.getScheme();
      }
    };
  }

  private ImageStore getHttpStore() throws URISyntaxException {
    return this.getStore(ImageStoreTest.HTTP_STORE_ROOT);
  }


  @Test
  public void testGetUri() throws Exception {
    URI expectedUri = new URI(ImageStoreTest.HTTP_STORE_ROOT + "/demo/test.jpg");
    URI actualUri = this.store.getImageUri("test.jpg", "demo");
    assertEquals(expectedUri, actualUri);
  }

  @Test
  public void testGetUriNoPrefix() throws Exception {
    URI expectedUri = new URI(ImageStoreTest.HTTP_STORE_ROOT + "/test.jpg");
    URI actualUri = this.store.getImageUri("test.jpg");
    assertEquals(expectedUri, actualUri);
  }
  @Test
  public void testGetUriWithSlash() throws URISyntaxException {
    URI expectedUri = new URI(ImageStoreTest.FILE_STORE_ROOT + "/scuba.jpg");
    URI actualUri = this.getFileStore().getImageUri("scuba.jpg");
    assertEquals(expectedUri, actualUri);
  }
}