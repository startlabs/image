package io.avata.imageStore;

import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Paul Daniel Iway
 */
public class LocalFileSystemImageStoreTest {

  private LocalFileSystemImageStore store;
  @Before
  public void setup() throws URISyntaxException {
    URI root = new URI("file://localhost/");
    this.store = new LocalFileSystemImageStore(root);
  }

  @Test(expected = InvalidStoreRootException.class)
  public void testGetImageUriNullScheme() throws Exception {

    LocalFileSystemImageStore testStore = new LocalFileSystemImageStore("/");
    URI uri = this.store.getImageUri("chewie.jpg");
    URI expectedUri = new URI("file://localhost/chewie.jpg");

    assertEquals(expectedUri, uri);
  }

  public void testGetImageUri() throws Exception {

    LocalFileSystemImageStore testStore = new LocalFileSystemImageStore("/");
    URI uri = this.store.getImageUri("file:/.chewie.jpg");
    URI expectedUri = new URI("file://localhost/chewie.jpg");

    assertEquals(expectedUri, uri);
  }

  @Test
  public void testGetScheme() throws Exception {
    assertEquals("file", this.store.getScheme());
  }

  @Test(expected = InvalidStoreRootException.class)
  public void testDifferentScheme() throws URISyntaxException {
    new LocalFileSystemImageStore("http://test.com/");
  }

  public void testCreate() {
    fail();
  }
}