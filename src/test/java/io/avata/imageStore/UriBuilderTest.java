package io.avata.imageStore;

import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paul Daniel Iway on 4/26/16.
 */
public class UriBuilderTest {

  public static final String LOCAL_PATH = "/home/paul/Pictures";
  public static final String LOCAL_PATH_SCHEMED = "file:///home/paul/Pictures";
  public static final String LOCAL_PATH_SCHEMED_HOST = "file://localhost/home/paul/Pictures";
  public static final String HTTP = "http://avata.io";
  public static final String HTTP_LONG_PATH = "http://avata.io/images";
  @Before
  public void setUp() throws Exception {

  }

  @Test
  public void setLocalPathSchemed() throws URISyntaxException {
    UriBuilder builder = UriBuilder.getBuilder(LOCAL_PATH_SCHEMED);
    assertEquals(new URI(LOCAL_PATH_SCHEMED), builder.build());
  }

  @Test
  public void localPathNoScheme() throws URISyntaxException {
    UriBuilder builder = UriBuilder.getBuilder(LOCAL_PATH);
    assertEquals(new URI(LOCAL_PATH), builder.build());
  }

  @Test
  public void localPathNoSchemeTestPath() throws URISyntaxException {
    UriBuilder builder = UriBuilder.getBuilder(LOCAL_PATH);
    builder.addPath("my_dir");
    builder.addPath("123.jpg");
    assertEquals(new URI(LOCAL_PATH + "/my_dir/123.jpg"), builder.build());
  }

  @Test
  public void httpPath() throws URISyntaxException {
    UriBuilder builder = UriBuilder.getBuilder(HTTP);
    builder.addPath("my_dir");
    builder.addPath("123.jpg");
    assertEquals(new URI(HTTP + "/my_dir/123.jpg"), builder.build());
  }

  @Test
  public void httpPathTrailing() throws URISyntaxException {
    UriBuilder builder = UriBuilder.getBuilder(HTTP_LONG_PATH + "/");
    builder.addPath("my_dir");
    builder.addPath("123.jpg");
    assertEquals(new URI(HTTP_LONG_PATH + "/my_dir/123.jpg"), builder.build());
  }

  @Test
  public void slashesInAddPath() throws URISyntaxException {
    UriBuilder builder = UriBuilder.getBuilder(LOCAL_PATH_SCHEMED_HOST + "/");
    builder.addPath("/test/my_dir/");
    builder.addPath("/123.jpg");
    assertEquals(new URI(LOCAL_PATH_SCHEMED_HOST + "/test/my_dir/123.jpg"), builder.build());
  }
}