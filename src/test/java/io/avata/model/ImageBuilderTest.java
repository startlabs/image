package io.avata.model;

import io.avata.TestFileLoader;
import org.apache.commons.imaging.ImageReadException;
import org.junit.Test;

import javax.imageio.ImageIO;

import static org.junit.Assert.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * @author Paul Daniel Iway
 */
public class ImageBuilderTest implements TestFileLoader{


  @Test(expected = FileNotFoundException.class)
  public void testBuilderWithDirectoryFile() throws IOException, ImageReadException {
    ImageBuilderFactory.from(new File("/"));
  }

  @Test
  public void testBuilderReturnsImage() throws IOException, ImageReadException, URISyntaxException {
    File file = this.load("yoda.jpeg");
    ImageBuilder builder = ImageBuilderFactory.from(file);
    assertNotNull(builder.build());
  }

  @Test
  public void testBuilderImageMetaData() throws IOException, ImageReadException, URISyntaxException {

    File file = this.load("yoda.jpeg");
    ImageBuilder builder = ImageBuilderFactory.from(file);
    Image image = builder.build();

    assertEquals(401, image.getHeight());
    assertEquals(600, image.getWidth());
    assertEquals("JPEG", image.getFormat());
    assertEquals("JPEG", image.getFormat());
    assertEquals("image/jpeg", image.getMimeType());
    assertFalse(image.isTransparent());
    assertFalse(image.isProgressive());
    assertEquals(24575, image.length());
  }

  @Test
  public void testBuilderImageName() throws IOException, ImageReadException, URISyntaxException {

    File file = this.load("yoda.jpeg");
    ImageBuilder builder = ImageBuilderFactory.from(file);
    builder.setName("test.jpeg");
    Image image = builder.build();

    assertEquals("test.jpeg", image.getName());
  }

  @Test
  public void testGetFormatFromName() throws URISyntaxException, IOException, ImageReadException {
    File file = this.load("yoda.jpeg");
    ImageBuilder builder = ImageBuilderFactory.from(file);
    assertEquals("jpeg", builder.getFormatFromName("yoda.jpeg"));
  }


}
