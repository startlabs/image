package io.avata.service;

import io.avata.model.Image;
import io.avata.processor.ImageProcessor;
import io.avata.utils.ImageUtil;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Paul Daniel Iway
 */
@RunWith(DataProviderRunner.class)
public class ImageServiceTest {

  private ImageService service;

  @Before
  public void setUp() {
    this.service = new ImageService();
  }

  @DataProvider
  public static Object[][] mapDataProvider() {

    // first set
    Map<String, String[]> map = new HashMap<>();
    map.put("w",new String[] {"10"});
    map.put("h",new String[] {"10"});

    Map<String, String> flatMap = new LinkedHashMap<>();
    flatMap.put("w","10");
    flatMap.put("h","10");

    // second set
    Map<String, String[]> map01 = new HashMap<>();
    map01.put("w",new String[] {"10"});
    map01.put("h",new String[] {"10"});
    map01.put("x",new String[] {"1"});
    map01.put("y", new String[]{"0"});
    map01.put("crop",new String[] {"true"});
    map01.put("scale",new String[] {"true"});

    Map<String, String> flatMap01 = new LinkedHashMap<>();
    flatMap01.put("w","10");
    flatMap01.put("h","10");
    flatMap01.put("x","1");
    flatMap01.put("y","0");
    flatMap01.put("crop","true");
    flatMap01.put("scale","true");

    return new Object[][] {
        { flatMap, map },
        { flatMap01, map01}
    };
  }

  @DataProvider
  public static Object[][] flatMapDataProvider() {

    Map<String, String[]> map = new HashMap<>();
    map.put("w",new String[] {"10"});
    map.put("h",new String[] {"10"});
    map.put("x",new String[] {"10"});
    map.put("y",new String[] {"10"});

    Map<String, String[]> rearrangedMap = new HashMap<>();
    rearrangedMap.put("h",new String[] {"10"});
    rearrangedMap.put("w",new String[] {"10"});
    rearrangedMap.put("y",new String[] {"10"});
    rearrangedMap.put("x",new String[] {"10"});


    return new Object[][] {
        { map, rearrangedMap }
    };
  }


  @Test
  public void testFromEmptyMap() throws IOException {
    Image image = mock(Image.class);
    BufferedImage bufferedImage = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);
    when(image.getBufferedImage()).thenReturn(bufferedImage);
    assertEquals(bufferedImage,
                 this.service.bufferedImageFromMap(
                     new HashMap<>(), image));
  }

  @Test
  public void testFromMap() throws IOException {
    Image image = mock(Image.class);
    ImageManipulationService manipulator = mock(ImageManipulationService.class);

    ImageUtil util = mock(ImageUtil.class);
    this.service.setImageUtil(util);

    Map<String, String[]> map = new HashMap<>();
    map.put("w",new String[] {"10"});
    map.put("h",new String[] {"10"});
    map.put("q",new String[] {"50"});

    when(manipulator.fromMap(this.service.flattenMap(map))).thenReturn(mock(ImageProcessor.class));
    this.service.setManipulator(manipulator);


    this.service.fromMap(map, image);
    verify(util).toByteArray(any(BufferedImage.class), any(String.class));
  }

  @Test
  @UseDataProvider("mapDataProvider")
  public void testFlattenMap(Map<String, String> flatMap, Map<String, String[]> testMap){
    Map<String, String> resultMap = this.service.flattenMap(testMap);
    assertTrue(resultMap.equals(flatMap));
  }

  @Test
  @UseDataProvider("flatMapDataProvider")
  public void testFlatMapHashcode(Map<String, String[]> map, Map<String, String[]> rearrangedMap) {
    Map<String, String> flatMap = this.service.flattenMap(map);
    Map<String, String> flatMapFromRearranged = this.service.flattenMap(rearrangedMap);
    assertEquals(flatMap.hashCode(), flatMapFromRearranged.hashCode());

  }
}